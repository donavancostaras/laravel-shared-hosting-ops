mysql-details = \
    --user='$(db-username)' \
    --password='$(db-password)' \
    --host='$(db-host)'

mysql-dump-cmd = mysqldump -e $(mysql-details) $(db-database)

mysql-cmd = mysql $(mysql-details)
mysql-db = --database=$(db-database)

mysql-clean-db-cmd = $(mysql-dump-cmd) --add-drop-table --no-data | \
  grep -e '^DROP \\| FOREIGN_KEY_CHECKS' | \
  $(mysql-cmd) $(mysql-db)
mysql-clean-db = $(mysql-ssh) "$(mysql-clean-db-cmd)"

mysql-create-db-string = CREATE DATABASE IF NOT EXISTS $(db-database) COLLATE 'utf8_general_ci'; \
  GRANT ALL ON $(db-database).* TO 'default'@'%';
mysql-create-db = $(mysql-ssh) "$(mysql-cmd) -e \"$(mysql-create-db-string)\""

mysql-dump-to-backup-file = $(mysql-ssh) "$(mysql-dump-cmd)" > $(backup-folder)/database.sql
mysql-restore-from-backup = $(mysql-ssh) "$(mysql-cmd) $(mysql-db)" < $(backup-folder)/database.sql

mysql-mail-dump-cmd = $(mysql-dump-cmd) \
    | gzip | uuencode dbbackup_$(client)-live.gz \
    | mail -s \"[backup] $(client) live site sql on $$(date)\" $(EMAIL)

db-migrate-cmd = $(artisan) migrate --force
migrate = $(ssh) '$(db-migrate-cmd)'

xampp-bin := /Applications/xampp/xamppfiles/bin/

local-mysql-cmd := $(xampp-bin)mysql \
    --user='root' --password=''

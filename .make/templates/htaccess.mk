define HTACCESS_FILE

<IfModule mod_rewrite.c>

    RewriteEngine On
    Options +FollowSymLinks

    <IfModule mod_negotiation.c>
        Options -MultiViews -Indexes
    </IfModule>

    # RewriteBase /
    # RewriteCond %{REMOTE_ADDR} !^196\.213\.86\.194
    # RewriteCond %{REQUEST_URI} !^/maintenance\.php$$
    # RewriteRule ^(.*)$$ http://%{HTTP_HOST}/maintenance.php [R=307,L]

    # Handle Authorization Header
    RewriteCond %{HTTP:Authorization} .
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} (.+)/$$
    RewriteRule ^ %1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>

endef
export HTACCESS_FILE

define ADMIN_BLADE_TEMPLATE
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#000000">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="$(shell $(call asset-filename,main.css))" rel="stylesheet">
        <title>Admin</title>
    </head>
    <body>
        <noscript>
            You need to enable JavaScript to run this app.
        </noscript>
        <div id="root"></div>
        <script src="$(shell $(call asset-filename,main.js))"></script>
    </body>
</html>
endef
export ADMIN_BLADE_TEMPLATE
